//Mengubah arrow function
 const golden = goldenFunction => { 
     console.log('Soal No.1 ')
    console.log("this is golden!!")
}

golden()

//object literal
const newFunction = function literal(firstName, lastName){
    return {
      firstName, lastName,
      fullName: function(){
          let data = `${firstName } ${lastName}`
        console.log('Soal No.2')
        console.log(data)
        return 
      }
    }
  }
   
  newFunction("William", "Imoh").fullName() 

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

//ES6
let{firstName, lastName, destination, occupation, spell} = newObject

console.log('Soal No.3')
console.log(firstName, lastName, destination, occupation, spell)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east];

console.log('soal no 4')
console.log(combined)

const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, 
${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`

console.log('Soal No.5 ')
console.log(before)
